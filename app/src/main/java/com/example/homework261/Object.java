package com.example.homework261;

import android.graphics.drawable.Drawable;

public class Object {
    Drawable image;
    String name;

    public Object(Drawable image, String name) {
        this.image = image;
        this.name = name;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
