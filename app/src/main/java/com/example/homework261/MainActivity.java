package com.example.homework261;


import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Object> objects = new ArrayList();
    ListView objectsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        objects.add(new Object("https://www.discovergreece.com/sites/default/files/styles/sc_1000x1035/public/2019-11/acropolis-museum_athens-57.jpg?itok=rbz7y_HC",
                "Музей"));
        objects.add(new Object("https://www.discovergreece.com/sites/default/files/styles/sc_1000x1035/public/2019-11/acropolis-museum_athens-57.jpg?itok=rbz7y_HC",
                "Музей"));
        objects.add(new Object("https://www.discovergreece.com/sites/default/files/styles/sc_1000x1035/public/2019-11/acropolis-museum_athens-57.jpg?itok=rbz7y_HC",
                "Музей"));
        objects.add(new Object("https://www.discovergreece.com/sites/default/files/styles/sc_1000x1035/public/2019-11/acropolis-museum_athens-57.jpg?itok=rbz7y_HC",
                "Музей"));

        GridView objectsList = (GridView) findViewById(R.id.gridview);
        ObjectAdapter objectAdapter = new ObjectAdapter(this, R.layout.item_layout, objects);
        objectsList.setAdapter(objectAdapter);

    }
}