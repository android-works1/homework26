package com.example.homework261;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ObjectAdapter extends ArrayAdapter<Object> {
    private LayoutInflater inflater;
    private int layout;
    private List<Object> objects;

    public ObjectAdapter(Context context, int resource, List<Object> objects) {
        super(context, resource, objects);
        this.objects = objects;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);

    }
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=inflater.inflate(this.layout, parent, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        TextView nameView = (TextView) view.findViewById(R.id.nameView);


        Object object = objects.get(position);


       // Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(imageView);
       // Picasso.get().load(object.getImageURL()).into(imageView);
        imageView.setImageResource(object.getImage());
        nameView.setText(object.getName());

        return view;
    }
}